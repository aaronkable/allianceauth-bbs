from ..models import Topic, Category
from django import template

register = template.Library()


@register.inclusion_tag('bbs/list_topics_body.html', takes_context=True)
def bbs_dashboard(context):
    categories = Category.objects.get_queryset().visible_for_user(context.request.user).values_list('pk', flat=True)
    topics = Topic.objects.filter(category__pk__in=categories)
    topics = topics\
        .select_related('created_by__profile__main_character')\
        .prefetch_related('posts', 'posts__created_by__profile__main_character')
    response = {
            'topics': topics,
        }
    return response

